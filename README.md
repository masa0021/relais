# README #

## What is this repository for? ##

* Using relay cards such as Conrad 197730: https://asset.conrad.com/media10/add/160267/c1/-/de/000197730IN01/information-197730-conrad-components-197730-relaiskarte-baustein-12-vdc.pdf

## Setup ##

### Software dependencies ###

* Linux
* python3
* python3-serial

### Hardware and software setup ###

* Serial connection, e. g. via USB/RS232 converter
* Apply correct privileges to /dev/...

## How to use relay card ##

* relay.py 6 0: Switch relay no 0

### Examples: Single relais card and switch relais 4 on ###

