#/usr/bin/env python3
#
# USAGE
# $> relay.py [COMMAND] [RELAY]
#
# Switch first relay on
# $> relay.py 6 0
#
# Switch third relay off
# $ conradRelay.py 7 2
#

import sys
import time
import serial
import binascii
import argparse


PORT = "/dev/ttyUSB0"
CARD = 1
#COMMAND = int(sys.argv[1])
#DATA = 0b1 << int(sys.argv[2])

MAXTRIES = 100

CMD_NOP         = 0
CMD_SETUP       = 1
CMD_GET_PORT    = 2
CMD_SET_PORT    = 3
CMD_GET_OPT     = 4
CMD_SET_OPT     = 5
CMD_SET_SINGLE  = 6
CMD_DEL_SINGLE  = 7
CMD_TOGGLE      = 8

BAUDRATE = 19200


def send(command, data):
    rawdata = bytearray([command, CARD, data])

    checksum = 0x00
    for rawbyte in rawdata:
        checksum ^= rawbyte
    rawdata.append(checksum)

    for retry in range(0, MAXTRIES):
        print("Command:")
        print(rawdata)

        ser.write(rawdata)
        time.sleep(1)
        response = ser.read(100)

        print("Response:")
        print(response)

        if response:
            return 0

        print("Failed! Retrying")
        time.sleep(1)

    print("Give up")
    return -1


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('-s', nargs='?', help='Which relay shall be switched on (0..7)', type=int, choices=range(0,7))
    parser.add_argument('-c', nargs='?', help='Which relay shall be switched off (0..7)', type=int, choices=range(0,7))
    args = parser.parse_args()

    if (args.c is None) and (args.s is None):
        parser.print_help()
        exit(1)

    # Initiate serial connection
    ser = serial.Serial(PORT, BAUDRATE, timeout=0, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE)

    # Initiate relay
    send(CMD_SETUP, 0)

    # Commands required a slight delay between init and set/clr
    time.sleep(1)

    # Send command to relay
    if(args.c is not None):
        send(CMD_DEL_SINGLE, 0b1 << args.c)

    if(args.s is not None):
        send(CMD_SET_SINGLE, 0b1 << args.s)
  
    # Close serial connection
    ser.close()


